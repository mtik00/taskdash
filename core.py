class Configuration:
    @classmethod
    def parse(cls, filename):
        """
        Given a filename of a YAML configuration, open the file, parse it and
        convert it into the appropriate data format.

        Also verify that all required fields are in it.
        """
        pass

class Api:
    @classmethod
    def download(self, configuration):
        """
        Given a configuration datastructure, download the necessary data from the resultsdb API.
        """
        pass

class Results:
    @classmethod
    def parse(self, db_results):
        """
        Given results from the resultsdb API, convert them into the appropriate
        data structure to be made into a report.
        """
        pass

class Report:
    """
    Generate a pretty report in HTML
    """
    def __init__(self, results):
        pass

    def __str__(self):
        return 'TBD'
