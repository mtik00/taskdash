from taskdash import yamlspec

simple_yaml = """---
- name: Default boot and Install
  testcases:
    - name: default_install_workstation_live_x32
      required: true

    - name: default_install_workstation_live_x64
      required: true
"""


class TestYamlSpec(object):

    def test_parse_one_dashboard(self):
        parsed = yamlspec.parse(simple_yaml)

        assert len(parsed) == 1

    def test_parse_to_dict(self):
        yamldict = yamlspec.parse(simple_yaml)[0]

        assert isinstance(yamldict, dict)

    def test_parse_sample_2_items(self):
        yamldict = yamlspec.parse(simple_yaml)[0]

        assert len(yamldict) == 2

    def test_parse_2_testcases(self):
        yamldict = yamlspec.parse(simple_yaml)[0]

        assert len(yamldict['testcases']) == 2
