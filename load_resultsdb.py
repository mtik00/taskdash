import resultsdb_api

"""
For whoever looks at this code, it is a dirty, hacky way to get some results into
resultsdb.

Don't take this code as an example of something that's wise or good to do
"""

mock_testsuites = {
        'openqa.workstation':
        {
            'live.base_selinux':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'bootiso.install_default':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            },
        'openqa.kde':
        {
            'live.base_selinux': 
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },

            'live.base_service_manipulation':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
            },
            },
        'openqa.server':
        {
            'dvd.base_selinux':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.base_service_manipulation':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.base_services_start':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'boot.install_default':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.default_upload':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.install_repository_nfs_graphical':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.install_repository_nfs_variation':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.realmd_join_cockpit':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.server_cockpit_basic':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.server_cockpit_default':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.server_firewall_default':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.server_realmd_join_kickstart':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.server_role_deploy_domain_controller':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            'dvd.support_server':
            {
                'x64_EFI':'PASSED',
                'x64_bios': 'PASSED',
                'aarch64': 'PASSED',
                },
            }
        }



def check_testcase(rdb, testcase_name):
    """check to see if the testcase exists, return boolean"""
    pass

def add_testcase(rdb, testcase_name):
    """add a testcase to resultsdb"""

def add_result(rdb, result_name, item, item_type, platform):
    """add a result to resultsdb. this is an attempt to codify the storage of
    install validation results, including platform (aarch64, x64_bios etc.)
    """
    pass


class ResultsdbLoader(object):

    def __init__(self, resultsdb_url):

        self.masterurl = "http://localhost/master"
        self.task_stepname = "some_stepname"
        self.execdb_server = "%s/jobs" % 'http://127.0.0.1/execdb'
        self.artifacts_baseurl = 'http://localhost/artifacts'

        if resultsdb_url is None:
            raise Exception("need to have a rdb url")

        self.resultsdb = resultsdb_api.ResultsDBapi(resultsdb_url)

        self._ensured_testcases = []

    def ensure_testcase_exists(self, name):
        """ Make sure that the testcase exists in resultsdb, otherwise create
        the testcase using a dummy url as a reference

        :param str name: name of the testcase to check for
        """

        if name in self._ensured_testcases:
            return

        try:
            self.resultsdb.get_testcase(name)
            self._ensured_testcases.append(name)
            return
        except resultsdb_api.ResultsDBapiException, e:
            if not e.message.startswith('Testcase not found'):
                raise e

        # since the testcase doesn't exist, create it with a dummy value for url
        # it can be updated later when it's not blocking results reporting
        dummyurl = 'http://faketestcasesRus.com/%s' % name
        self.resultsdb.create_testcase(name, dummyurl)
        self._ensured_testcases.append(name)

    def create_resultsdb_job(self, name, uuid=None, refurl=None):
        """ Create a job in resultsdb for reporting results against

        :param str name: name of job to report against
        :param str uuid: UUID of the job (most probably provided by ExecDB)
        :param str refurl: url pointing to the execution overview.
           If set to None, ExecDB url is created from UUID
        :returns: dict of rendered json results
        """

        url = "%s/%s" % (self.execdb_server, uuid)
        if refurl is not None:
            url = refurl

        jobdata = self.resultsdb.create_job(url, status='SCHEDULED', name=name,
                                            uuid=uuid)
        self.resultsdb.update_job(id=jobdata['id'], status='RUNNING')

        return jobdata

    def complete_resultsdb_job(self, jobid):
        """ Change the resultsdb job to a status of ``COMPLETED``, indicating
        that the reporting is complete.

        :param int jobid: The resultsdb jobid to be modified
        :returns: json representation of returned data from the resultsdb instance
        """
        return self.resultsdb.update_job(jobid, status='COMPLETED')

    def get_artifact_path(self, artifactsdir, artifact):
        """Return the relative path of :attr str artifact: inside the
        :attr str artifactsdir:.
        :returns: relative path to the artifact file or None, if the file
                  does not exist, or is outside the artifactsdir.
        """
        artifactsdir = os.path.realpath(artifactsdir)
        if os.path.isabs(artifact):
            artifact_path = artifact
        else:
            artifact_path = os.path.join(artifactsdir, artifact)
        artifact_path = os.path.realpath(artifact_path)

        if not os.path.exists(artifact_path):
            log.warn('Artifact %r does not exist, ignoring' % artifact_path)
            return None
        elif not artifact_path.startswith(artifactsdir):
            log.warn('Artifact %r is placed outside of artifacts directory %r, ignoring',
                     artifact_path, artifactsdir)
            return None

        return os.path.relpath(artifact_path, start=artifactsdir)

    def process(self, params, arg_data):
        # checking if reporting is enabled is done after importing yaml which
        # serves as validation of input results

        checkname = '%s.%s' % (arg_data['namespace'], arg_data['checkname'])

        # find out if the task is allowed to post results into the namespace
        if config.get_config().profile == config.ProfileName.PRODUCTION:
            self.check_namespace(checkname, arg_data)

        # for now, we're creating the resultsdb job at reporting time
        # the job needs to be 'RUNNING' in order to append any results
        job_url, log_url = buildbot_utils.get_urls(arg_data['jobid'],
                                                   self.masterurl,
                                                   self.task_stepname)

        job_data = self.create_resultsdb_job(checkname, uuid=arg_data['uuid'])

        log.info('Posting %s results to ResultsDB...' % len(check_details))
        for detail in check_details:
            checkname = '%s.%s' % (arg_data['namespace'],
                                   detail.checkname or arg_data['checkname'])

            self.ensure_testcase_exists(checkname)
            result_log_url = log_url
            if detail.artifact:
                artifact_path = self.get_artifact_path(
                    arg_data['artifactsdir'],
                    detail.artifact
                    )
                if artifact_path:
                    result_log_url = "%s/all/%s/task_output/%s" % (
                        self.artifacts_baseurl,
                        arg_data['uuid'],
                        artifact_path,
                        )
            try:
                result = self.resultsdb.create_result(
                    job_id=job_data['id'],
                    testcase_name=checkname,
                    outcome=detail.outcome,
                    summary=detail.note or None,
                    log_url=result_log_url,
                    item=detail.item,
                    type=detail.report_type,
                    **detail.keyvals)
                detail._internal['resultsdb_result_id'] = result['id']

            except resultsdb_api.ResultsDBapiException, e:
                log.error(e)
                log.error("Failed to store ResulsDB: `%s` `%s` `%s`",
                           detail.item, checkname, detail.outcome)

        # move job to completed
        self.complete_resultsdb_job(job_data['id'])

        return check.export_YAML(check_details)

    def load_data(self, testsuites):

        for suite in testsuites:

            for case in testsuites[suite]:

                for result in testsuites[suite][case]:

                    job_data = self.create_resultsdb_job('openqatest', uuid='')
                    try:
                        item = "somecompose-1.2-3"
                        item_type = 'compose'
                        platform = result
                        testcase_name = "{}.{}".format(suite, case)
                        print("creating a result for {} ({}, {}, {}, {})".format(testcase_name, result, item, item_type, platform))
                        result = self.resultsdb.create_result(
                            job_id=job_data['id'],
                            testcase_name=testcase_name,
                            outcome=testsuites[suite][case][result],
                            summary='fake data for demo purposes',
                            log_url='http://127.0.0.1/somefakelog',
                            item=item,
                            type=item_type,
                            platform=result)

                    except resultsdb_api.ResultsDBapiException, e:
                        print("problem when posting results")
                        raise e


if __name__ == "__main__":
    rdb = ResultsdbLoader('http://localhost:5001/api/v1.0')
    rdb.load_data(mock_testsuites)
