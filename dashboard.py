"""
Connect to ResultsDB and create a pretty HTML report based on a YAML configuration file.
"""
from core import Configuration, Api, Results, Report
import argparse

parser = argparse.ArgumentParser(description='Create a HTML report from resultsdb')
parser.add_argument('config', help='configuration file for report')
args = parser.parse_args()

config_filename = args.config

configuration = Configuration.parse(config_filename)
database_results = Api.download(configuration)
results = Results.parse(database_results)
report = Report(results)
print report
