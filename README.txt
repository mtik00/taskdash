Dashboard
    value/desires
        to display test results for non-technical audiences
        results are available at a shareable URL
        "which results" is defined by a YAML configuration file
        data from the resultsdb API is filtered based on the configuration in the YAML file
        data from the resultsdb API is transformed into an appropriate format for display
        timeliness
            should execute in under a minute
            should be runnable once a day or once per hour at minimum
    examples
        show result of single test
            inputs
                configuration file
                    URL of database
                    name of test
            database state
                number of results for this test
                    no results
                    one result
                    many results
                may have other tests in database
                number of item tag variations
                    only one
        show result of single test with many item tags
            database state
                number of results for this test
                    many results
                results have varying item tags
                results have same item tags
                there will always be key-value pairs of item and itemtype
        show result of several tests, ignoring item tags
            number of results in database
                none
                one
                many
        show results with several tests and several item tags
    RPi
        passwords
    end users
        users of charts
            non-technical
            wants overview
        user of tool
    failure states
        no results available
        parsing YAML error
        resultsdb server not available
            500 error
            404 error
            or being dead
